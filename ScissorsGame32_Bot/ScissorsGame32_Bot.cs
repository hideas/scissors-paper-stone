﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScissorsGame32_Bot
{
    public partial class ScissorsGame32_Bot : Form
    {
        private static TelegramBotClient _telegramBotClient;
        private string _botName;
        private User _user;
        private static string _token = "...";

        public ScissorsGame32_Bot()
        {
            InitializeComponent();

            // tbEnter.Text = "976723650:AAGOujVpGRjUOnYElOh5ruUMLmllX4SR_VA";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbEnter.Text))
            {
                _token = tbEnter.Text;
            }
            _telegramBotClient = new TelegramBotClient(_token);
            _telegramBotClient.OnMessage += _telegramBotClient_OnMessage;

            // Запуск бота
            _telegramBotClient.SetWebhookAsync("https://{DESKTOP-CF4HGLA}:8443/WebHook");
            // "https://{DESKTOP-CF4HGLA}:8443/WebHook"

            // Принудительный опрос бота
            _user = _telegramBotClient.GetMeAsync().Result;

            // Знакомство (необяязательно)
            _botName = _user.Username;

            // Запуск процесса постоянного асинхронного прослушивания
            _telegramBotClient.StartReceiving();
        }

        private async void _telegramBotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            // Получение сообщения от собеседника
            Telegram.Bot.Types.Message message = e.Message;
            if (message == null || message.Type != Telegram.Bot.Types.Enums.MessageType.Text)
            {
                return;
            }

            string answer = null;

            if (message.Text.Equals("/start"))
            {
                string firstName = message.From.FirstName;
                string lastName = message.From.LastName;
                string userName = message.From.Username;

                answer = $"{firstName} {lastName}, сыграем в игру 'Камень-ножницы-бумага'?\n/stone - твой камень\n/scissors - твои ножницы\n/paper - твоя бумага";
            }
            if (message.Text.Equals("/stone"))
            {
                answer = "А у меня бумага - ты проиграл";
            }
            if (message.Text.Equals("/scissors"))
            {
                answer = "А у меня камень - ты проиграл";
            }
            if (message.Text.Equals("/paper"))
            {
                answer = "А у меня ножницы - ты проиграл";
            }

            await _telegramBotClient.SendTextMessageAsync(message.Chat.Id, answer);
        }
    }
}
